import axios from "axios";
import { Cup } from "./entities";


export async function fetchOneCup(id: number | string) {
    const response = await axios.get<Cup>('/api/cups' +id);
    return response.data;
}
export async function postCup(cup:Cup) {
    const response = await axios.post<Cup>('http://localhost:8000/api/cups');
    return response.data;
}