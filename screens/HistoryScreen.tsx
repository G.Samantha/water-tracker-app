import React from 'react';
import {Dimensions, StyleSheet, View} from "react-native";
import {Title} from "react-native-paper";
import {CalendarList} from "react-native-calendars";
import DateData from './DateData';




export default function HistoryScreen(){
        const [marked, setMarked] = React.useState({});
        const [waterObject, setWaterObject] = React.useState({});
        const [selected, setSelected] = React.useState(null);
        
    return(
        <View style={styles.container}>
        <Title>Water intake history</Title>
        <View style={styles.calendar}>
            <CalendarList
                theme={{
                    calendarBackground: '#131A26',
                    textSectionTitleColor: '#ffffff',
                    selectedDayTextColor: '#ffffff',
                    selectedDayBackgroundColor: '#2176FF',
                    dayTextColor: '#ffffff',
                    monthTextColor: '#ffffff',
                    textMonthFontWeight: 'bold',
                }}
            />
        </View>
        <View style={styles.content}>
        <DateData
            date={selected}
            chartData={waterObject}
        />
        </View>
        
    </View>)};
    

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'column',
            marginTop: 20,
            alignItems: 'center',
            
        },
        calendar: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-evenly',
            
        },
        content: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-evenly',
            
        },
        buttons: {
            flex: 0,
            flexDirection: 'row',
            width: Dimensions.get('window').width,
            justifyContent: 'space-evenly',
        },
    });