import * as Notifications from "expo-notifications";
import { Button, Provider as PaperProvider } from "react-native-paper";
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

const screenWidth = Dimensions.get("window").width;

async function scheduleNotification() {
  await Notifications.requestPermissionsAsync().then(() => {
    Notifications.scheduleNotificationAsync({
      content: {
        title: "💧Notification💧",
        subtitle: "Aller hophop on picole !",
      },
      trigger: {
        repeats: true,
        seconds: 1800,
      },
    });
  });
}

export default function Notification() {
  return (
    <>
      <Button
        style={[
          styles.notificationButton,
          {
            backgroundColor: "#74ccf4",
          },
        ]}
        mode="contained"
        onPress={() => scheduleNotification()}
      >
        <Text style={styles.notificationText}>Activier Notification</Text>
      </Button>
      <Button
        style={[
          styles.notificationButton,
          {
            backgroundColor: "red",
          },
        ]}
        mode="contained"
        onPress={() => Notifications.cancelAllScheduledNotificationsAsync()}
      >
        <Text style={styles.notificationText}>Désactiver Notifications</Text>
      </Button>
    </>
  );
}

const styles = StyleSheet.create({
  notificationButton: {
    height: 50,
    justifyContent: "space-evenly",
    padding: 3,
    margin: 2,
    flexDirection: "row",
    alignItems: "center",
    width: screenWidth - 150,
    alignContent: "space-between",
    flexWrap: "wrap",
  },
  notificationText: { color: "white", fontWeight: "500", fontSize: 16 },
});
