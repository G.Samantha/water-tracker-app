import React, { useContext, useState } from "react";
import { Button, Dialog, Portal, Provider, Text, TextInput } from "react-native-paper";
import { WaterCupContext } from "../auth/volume-context";

export default function SettingsDialog(props: any) {
//   const { volume: oldCupVolume } = useContext(WaterCupContext);
const { setVolume } = useContext(WaterCupContext);
  const [bottleVolume, setBottleVolume] = useState('0');
  const [cupVolume, setCupVolume] = useState<any>('0');
  const [oldBottleVolume, setOldBottleVolume] = useState('0');
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

 const updateWaterCup = (cupVol: string) => {
    const link:any = setCupVolume(cupVol);

    setVolume(link);



    console.log("link"+link);
    console.log("setting cup volume = " + cupVol);
    console.log("volume"+ cupVol);
  }

 
  
//   function updateWaterBottle(volume: string) {
//     setBottleVolume(volume);
//     console.log(volume);
//   }

  return (
    <>
      <Dialog
        visible={props.isDialogVisible}
        onDismiss={() => props.setIsDialogVisible(false)}
        style={props.style}
      >
        <Dialog.Title>Settings</Dialog.Title>
        <Dialog.Content style={{ alignContent: "space-around" }}>
          <Text>
            Here you can customize the volumes of the water cup and bottle to
            match your own to make adding water quick and easy.
          </Text>

          <Text>{`
                        Current volumes:
                        Water cup: ${cupVolume} ml
                        Water bottle: ${oldBottleVolume} ml`
          }</Text>

          <TextInput
            style={{ marginTop: 30 }}
            label="Volume of your water cup"
            placeholder="in millilitres"
            underlineColor="#2176FF"
            theme={{ colors: { primary: "#2176FF" } }}
            onChangeText={text => setCupVolume(text)}
            keyboardType="numeric"
          />
          <TextInput
            style={{ marginTop: 60 }}
            label="Volume of your water bottle"
            placeholder="in millilitres"
            underlineColor="#2176FF"
            theme={{ colors: { primary: "#2176FF" } }}
            onChangeText={text => setBottleVolume(text)}
            keyboardType="numeric"
          />
        </Dialog.Content>
        <Dialog.Actions>
          <Button
            theme={{ colors: { primary: "#2176FF" } }}
            onPress={() => {
                setVolume(cupVolume);
                props.setIsDialogVisible(false);
                console.log(" onpress"+  cupVolume);
                

              }
            }
            // onPress={() => {
            //     updateWaterCup(cupVolume);
            //     props.setIsDialogVisible(false);
            //   }}
          >
            Done
          </Button>
        </Dialog.Actions>
      </Dialog>
    </>
  );
}





































// import React, { useContext, useState } from "react";
// import { Button, Dialog, Portal, Provider, Text, TextInput } from "react-native-paper";
// import waterCupContext from "../auth/volume-context";



// export default function SettingsDialog(props: any) {

//     const [bottleVolume, setBottleVolume] = React.useState<any>(0);
//     const [cupVolume, setCupVolume] = React.useState<any>(0);
//     const [oldBottleVolume, setOldBottleVolume] = React.useState<any>(0);
//     const [oldCupVolume, setOldCupVolume] = React.useState<any>(0);
//     const [visible, setVisible] = React.useState(false);
//     const openMenu = () => setVisible(true);
//     const closeMenu = () => setVisible(false);
 



    
//     function updateWaterCup(cupVol:any):any{
//         setCupVolume(cupVol); 
//         let tab = [...cupVol]
//         console.log("setting cup volume = " + cupVolume);
//     }

//     function updateWaterBottle(volume:any){
//         setBottleVolume(volume); 
//         console.log(bottleVolume);
//     }
    


//     return (
//         <>
//                     <Dialog
//                         visible={props.isDialogVisible}
//                         onDismiss={() => props.setIsDialogVisible(false)}
//                         style={props.style}>
//                         <Dialog.Title>Settings</Dialog.Title>
//                         <Dialog.Content style={{ alignContent: 'space-around' }}>
//                             <Text>Here you can customize the volumes of the water cup and bottle
//                                 to match your own to make adding water quick and easy.</Text>

//                             <Text>{`
//                             Current volumes:
//                             Water cup: ${oldCupVolume} ml
//                             Water bottle: ${oldBottleVolume} ml`
//                             }</Text>

//                             <TextInput
//                                 style={{ marginTop: 30 }}
//                                 label="Volume of your water cup"
//                                 placeholder="in millilitres"
//                                 underlineColor="#2176FF"
//                                 theme={{ colors: { primary: '#2176FF' } }}
//                                 onChangeText={text => setCupVolume(text)}
//                                 keyboardType='numeric'
//                             />
//                             <TextInput
//                                 style={{ marginTop: 60 }}
//                                 label="Volume of your water bottle"
//                                 placeholder="in millilitres"
//                                 underlineColor="#2176FF"
//                                 theme={{ colors: { primary: '#2176FF' } }}
//                                 onChangeText={text => setBottleVolume(text)}
//                                 keyboardType='numeric'
//                             />
//                         </Dialog.Content>
//                         <Dialog.Actions>
//                             <Button
//                                 theme={{ colors: { primary: '#2176FF' } }}
//                                 onPress={() => {
//                                     props.setIsDialogVisible(false);
//                                     <waterCupContext.Provider value={{volume: updateWaterCup(cupVolume)}}/>
                                    
//                                 }}>Done
//                                 </Button>
//                         </Dialog.Actions>
//                     </Dialog>

                   
                    



//         </>
//     )

// }

