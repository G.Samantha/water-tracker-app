import React, { useState } from 'react';
import { Provider, Portal, Dialog, Button, TextInput } from "react-native-paper";
import { ViewStyle } from 'react-native';

type Props = {
  isDialogVisible: boolean;
  setIsDialogVisible: (visible: boolean) => void;
  setTarget: (target: number) => void;
  style?: ViewStyle;
};

export default function ChangeTargetDialog(props: Props) {
  const [inputVal, setInputVal] = useState<number | ''>('');

  return (
    
      
        <Dialog
          visible={props.isDialogVisible}
          onDismiss={() => props.setIsDialogVisible(false)}
          style={props.style}
        >
          <Dialog.Title>Water target</Dialog.Title>
          <Dialog.Content>
            <TextInput
              label="Set water target"
              placeholder="in millilitres"
              underlineColor="#2176FF"
              theme={{colors: {primary: '#2176FF'}}}
              onChangeText={text => setInputVal(parseInt(text) || '')}
              keyboardType='numeric'
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button
              theme={{colors: {primary: '#2176FF'}}}
              
              onPress={() => {
                props.setIsDialogVisible(false);
                if (typeof inputVal === 'number') props.setTarget(inputVal);
              }}
            >
              Done
            </Button>
          </Dialog.Actions>
        </Dialog>
  );
}
