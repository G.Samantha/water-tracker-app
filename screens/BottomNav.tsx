import React from 'react';
import { BottomNavigation, Text} from 'react-native-paper';
import MainScreen from "./MainScreen";
import HistoryScreen from "./HistoryScreen";
import Notification from './Notification';


const Bottomnav = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'main', title: 'Main', focusedIcon: 'home' },
        { key: 'history', title: 'History', focusedIcon: 'chart-bell-curve' },
        { key: 'notif', title: 'Notifications', focusedIcon: 'bell' },
    ]);

    const renderScene = BottomNavigation.SceneMap({
        main: MainScreen,
        history: HistoryScreen,
        notif : Notification,
    });

    return (
        <BottomNavigation
            navigationState={{ index, routes }}
            onIndexChange={setIndex}
            renderScene={renderScene}
            shifting={true}
        />
    );
};

export default Bottomnav;