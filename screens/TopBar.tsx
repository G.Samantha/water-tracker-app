import React from 'react';
import {Appbar, Dialog, Divider, Menu, Portal, Provider, Text, Button} from 'react-native-paper';
import SettingsDialog from './SettingsDialog';
import {TextInput, View } from 'react-native';
import DialogTitle from 'react-native-paper/lib/typescript/src/components/Dialog/DialogTitle';





export default function TopBar(){
    const [visible, setVisible] = React.useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    
    const [isDialogVisible, setIsDialogVisible] = React.useState(false);
    const showDialog = () => setIsDialogVisible(true);
    const hideDialog = () => setIsDialogVisible(false);


    return(
        <>
            


                    <Appbar.Header>
                        {/* <Appbar.Action style={{ flex: 0 }} onPress={() => { }} /> */}
                        <Appbar.Content style={{ alignItems: 'center'}} title="Water Intake Tracker" />

                        <Menu
                            visible={visible}
                            onDismiss={closeMenu}
                            anchor={<Appbar.Action style={{ flex: 0 }} color="black" icon="dots-vertical" onPress={() => { openMenu() }} />}>
                                {/* <Menu.Item onPress={() => {resetWater(); closeMenu();}} title="Reset water intake" /> */}
                            
                            <Divider />
                                
                            <Menu.Item onPress={() => {showDialog(); closeMenu();}} title="Settings" />
                        </Menu>


                            <Portal>
                                <SettingsDialog
                                    isDialogVisible={isDialogVisible}
                                    setIsDialogVisible={setIsDialogVisible}
                                    hideDialog={hideDialog}
                                />
                            </Portal>


                        

                    </Appbar.Header>
            

        </>
    )
}
