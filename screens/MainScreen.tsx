import React, { useContext} from 'react';
import { View, StyleSheet, Dimensions} from "react-native";
import { Title, Text, Button, Chip, Snackbar, Portal, Provider} from "react-native-paper";
import { AnimatedCircularProgress } from "react-native-circular-progress";



import ChangeTargetDialog from './ChangeTargetDialog';
import {WaterCupContext, WaterCupProvider} from '../auth/volume-context';

const screenWidth = Dimensions.get("window").width;

export default function HomeScreen() {

    const [target, setTarget] = React.useState(1000);
    const [targetReach, setTargetReach] = React.useState(false);

    const [targetSnackVisible, setTargetSnackVisible] = React.useState(false);
    const onToggleTargetSnackBar = () => setTargetSnackVisible(true);
    const onDismissTargetSnackBar = () => setTargetSnackVisible(false);

    const [isTargetDialogVisible, setIsTargetDialogVisible] = React.useState(false);
    const [isCustomDialogVisible, setIsCustomDialogVisible] = React.useState(false);

    const [percentage, setPercentage] = React.useState(0);
    const [water, setWater] = React.useState(0);  // => Millitlitres //

    const [waterCup, setWaterCup] = React.useState(10);
    const [waterBottle, setWaterBottle] = React.useState(600);

    const cupVol = useContext(WaterCupContext);
    

    const addCup=(amount:number)=>{
        const values = Math.floor((waterCup*100)/target);
        if(amount){
            setPercentage(percentage+values);
            setWater(cupVol.volume + water)
            console.log('addCup = '+ cupVol.volume);

            if (percentage >= 100){
                // console.log("Target reached!")
                setTargetReach(true);
                onToggleTargetSnackBar();
            }
        }
    }

    const addBottle=(amount:number)=>{
        const values = Math.floor((waterBottle*100)/target);
        if(amount){
            console.log(cupVol);
            setPercentage(percentage+values);
            setWater(waterBottle+water);

            if (percentage >= 100){
                console.log("Target reached!")
                setTargetReach(true);
                onToggleTargetSnackBar();
            }
        }
    }

    
    const addWater=(amount:number)=>{
        console.log(percentage);
    }

    const defineTarget=(inputVal:any)=>{
        setTarget(inputVal); 
    }


    const valuesToPercentage=(target:any, current:any)=> {
        return Math.floor(100 * (current / target));
        
    }

    

    return (
        <WaterCupProvider>
            <View style={styles.container}>

                <Title>Today</Title>

                <Chip
                    mode='outlined'
                    icon='information'
                    selectedColor='#2176FF'
                    style={{ marginTop: 10 }}
                    onPress={() => setIsTargetDialogVisible(true)}>
                    Water target: {target} ml
                </Chip>

            

                <View style={styles.content}>  

                    <AnimatedCircularProgress
                    style={styles.progress}
                    size={245}
                    width={32}
                    rotation={0.25}
                    arcSweepAngle={360}
                    fill={percentage}
                    tintColor="#2176FF"
                    backgroundColor="#caf0f8"
                    onAnimationComplete={() => console.log('onAnimationComplete')}
                    childrenContainerStyle={styles.circle}
                    children={
                        () => (
                            <View style={ {alignItems: 'center', transform: [{ rotate: "-45deg"}],}}>
                                <Title style={{color: 'white'}}>
                                    {cupVol[0]} ml
                                </Title>
                                <Text style={{color: 'white'}}>
                                    {percentage} %
                                </Text>
                            </View>
                        )
                    }
                />

                    <View style={styles.addContainer}>
                        <Title style={{ marginHorizontal: 70 }}>+ Add a portion of water</Title>
                            <View style={styles.buttons}>
                            
                                <Button icon="cup" mode="contained" onPress={() => addCup(cupVol)} >
                                    Cup
                                </Button>

                                <Button icon="glass-stange" mode="contained" onPress={() => addBottle(waterBottle)}>
                                    Bottle
                                </Button>
                                <Button icon="water" mode="contained" onPress={() => setIsCustomDialogVisible(true)}>
                                    Something else
                                </Button>
                            </View>
                    </View>

                </View>

                <Snackbar
                visible={targetSnackVisible}
                duration={2500}
                onDismiss={onDismissTargetSnackBar}
                theme={{ colors: { surface: '#FFFFFF', onSurface: '#FDCA40', accent: '#FFFFFF'}}}
                action={{
                    label: 'Yay!',
                    // onPress: () => onDismissTargetSnackBar()
                }}>Congrats, you reached your water intake goal!</Snackbar>



            <Portal>
                <ChangeTargetDialog
                    isDialogVisible={isTargetDialogVisible}
                    setIsDialogVisible={setIsTargetDialogVisible}
                    setTarget={defineTarget}
                />
            </Portal>

        </View>
        </WaterCupProvider>
    )

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 20,  
    },
    content: {
        flex: 1,
        marginTop: 50,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        
        
    },
    addContainer: {
        flex: 1,
        flexGrow: 0.45,
        flexDirection: 'row',
        alignItems: 'center',
        width: screenWidth,
        alignContent: 'space-between',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
      
    },
    buttons: {
        flexDirection: 'row',
        alignItems: 'center',
        width: screenWidth - 100,
        alignContent: 'space-between',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
        
    },
    circle: {
        width: 181,
        height: 181,
        borderRadius: 120,
        borderWidth: 5,
        backgroundColor: '#27354d',
        borderColor: "#0051d4",
        borderTopLeftRadius: 10,
        borderBottomWidth: 10,
        borderRightWidth: 10,
        transform: [{ rotate: "45deg" }],
        shadowColor: "#000000",
        shadowOffset: {
            width: 1,
            height: 1,
        },
        shadowOpacity: 0.9,
        shadowRadius: 10.00,
        elevation: 10,
       
    },
    progress: {
        width: 264,
        height: 264,
        marginBottom: 10,
        borderRadius: 300,
        borderWidth: 10,
        borderColor: "#0051d4",
        overflow: 'hidden',
        color: '#caf0f8'
        
    }
    
});