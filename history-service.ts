import axios from "axios";
import { History } from "./entities";


export async function fetchOneHistory(id: number | string) {
    const response = await axios.get<History>('/api/histories' +id);
    return response.data;
}
export async function postHistory(history:History) {
    const response = await axios.post<History>('/api/histories', history);
    return response.data;
}
