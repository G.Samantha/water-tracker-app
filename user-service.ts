import axios from "axios";
import { User } from "./entities";


export async function fetchOneUser(id: number | string) {
    const response = await axios.get<User>('/api/cups' +id);
    return response.data;
}
export async function postUser(user:User) {
    const response = await axios.post<User>('/api/cups', user);
    return response.data;
}