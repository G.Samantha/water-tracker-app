export interface User {
    id?: number;
    name?: string
}
export interface History {
    id?: number;
    quantity?: string
}
export interface Cup {
    id?: number;
    quantity?: string
}
export interface Bottle {
    id?: number;
    quantity?: string
}

