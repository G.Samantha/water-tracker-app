import axios from "axios";
import { Bottle } from "./entities";


export async function fetchOneBottle(id: number | string) {
    const response = await axios.get<Bottle>('/api/bottles' +id);
    return response.data;
}
export async function postBottle(bottle:Bottle) {
    const response = await axios.post<Bottle>('/api/bottles', bottle);
    return response.data;
}