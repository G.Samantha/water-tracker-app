import React, { createContext, useState } from 'react';

export const WaterCupContext = createContext<any>({
  volume: '0',
  setVolume: () => { },
});

export const WaterCupProvider = (props:any) => {
  const [volume, setVolume] = useState('0');
  const value = { volume, setVolume };

  return (
    <WaterCupContext.Provider value={value}>
      {props.children}
    </WaterCupContext.Provider>
  );
};
