import { StatusBar } from "expo-status-bar";
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import BottomNav from "./screens/BottomNav";
import MainScreen from "./screens/MainScreen";
import TopBar from "./screens/TopBar";
import {
  Button,
  MD3LightTheme,
  Provider as PaperProvider,
} from "react-native-paper";
import Notification from "./screens/Notification";
import { white } from "react-native-paper/lib/typescript/src/styles/themes/v2/colors";


const screenWidth = Dimensions.get("window").width;

export default function App() {


  const theme = {
    ...MD3LightTheme, // or MD3DarkTheme
    roundness: 15,
    colors: {
      ...MD3LightTheme.colors,
      primary: "#0066FF",
      accent: "#33A1FD",
      // surface: "#457b9d",
      background: "#e5d9f2",
      text: "#FFFFFF",
      
    },
  };


  return (
    <PaperProvider theme={theme} >
      <View style={styles.container}>
        <View style={styles.content}>
          <TopBar />
          {/* <MainScreen /> */}
          <BottomNav/>
        </View>
        <StatusBar style="auto" />
      </View>
    </PaperProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    width: screenWidth,
  },
  content: {
    flex: 10,
  },
});
